require 'redis'
require 'json'
require 'pry'
require 'securerandom'


$counterBackend = 0
$counterRails = 0

NamespacedQueue = Struct.new(:namespace, :queue)

class BackendTaskProcessor
  def call
    redis = Redis.new
    redis.subscribe('channel') do |on|
      on.message do |channel, raw_message|
        # puts "BackendTaskProcessor-> #{channel} - #{message}"

        # repush to 'channel-response'
        $counterBackend += 1
        message = JSON.parse(raw_message, symbolize_names: true)

        redis.publish(
          'channel-response',
          {
            namespace: message[:namespace],
            message: "BackendTaskProcessor-> #{message[:message]}"
          }.to_json
        )
      end
    end
  end
end


Thread.new do
  BackendTaskProcessor.new.call
end

sleep(0.5)

class BackendRailsProcessor
  def initialize
    @namespaced_queues = []
  end

  def call
    Thread.new do
      redis = Redis.new

      redis.subscribe('channel-response') do |on|
        on.message do |channel, raw_message|
          message = JSON.parse(raw_message, symbolize_names: true)

          @namespaced_queues.each do |queue|
            queue.queue << message[:message] if queue.namespace == message[:namespace]
          end
        end
      end
    end
  end

  def wait_for_mesg(queue)
    @namespaced_queues << queue
  end

  def remove_queue(queue)
    @namespaced_queues.delete(queue)
  end
end

class PublishService
  def call(namespace, msg)
    redis = Redis.new
    redis.publish('channel', { namespace: namespace, message: msg }.to_json)
  end
end


$brp_instance = BackendRailsProcessor.new
$brp_instance.call

class ListenForResponseService
  def initialize(namespace)
    @queue = Queue.new
    @namespaced_queue = NamespacedQueue.new(namespace, @queue)
    $brp_instance.wait_for_mesg(@namespaced_queue)
  end

  def call
    yield @queue.pop
    $brp_instance.remove_queue(@namespaced_queue)
  end
end

class ControllerOne
  attr_reader :counter

  def initialize
    @counter = 0
  end

  def call
    namespace = SecureRandom.uuid
    listen_serive = ListenForResponseService.new(namespace)
    PublishService.new.call(namespace, 'hello world one')

    listen_serive.call do |msg|
      # puts "Controller1-> #{msg}"
      @counter += 1
    end
  end
end


puts 'Start ====================='

th1 = Thread.new do
  time1 = Time.now
  controller = ControllerOne.new
  1000.times do |i|
    controller.call
  end
  puts "Time: #{Time.now - time1}, counter: #{controller.counter}"
end


th2 = Thread.new do
  time1 = Time.now
  controller = ControllerOne.new
  1000.times do |i|
    controller.call
  end
  puts "Time: #{Time.now - time1}, counter: #{controller.counter}"
end



th1.join
th2.join

puts 'End ======================='
