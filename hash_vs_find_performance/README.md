# Hash is much faster for lookups that array
## Was that even unexpected?

Three samples for different amounts of records
```
Array: 6.385558926995145
Hash: 0.004691161011578515
Hash was faster by 1361.1894606121155
 == Test 2 ==
                 user     system      total        real
array        0.753336   0.000000   0.753336 (  0.753365)
hash         0.000498   0.000000   0.000498 (  0.000499)
```
```
Array: 0.07271407899679616
Hash: 0.001870773994596675
Hash was faster by 38.86844653967556
 == Test 2 ==
                 user     system      total        real
array        0.770572   0.000073   0.770645 (  0.770664)
hash         0.000379   0.000020   0.000399 (  0.000397)
```
```
Array: 0.01189776198589243
Hash: 0.0016067890101112425
Hash was faster by 7.404682202219391
 == Test 2 ==
                 user     system      total        real
array        0.742030   0.000000   0.742030 (  0.742064)
hash         0.000412   0.000000   0.000412 (  0.000411)
```