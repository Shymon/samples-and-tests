require 'benchmark'
require 'pry'


class Record
  attr_reader :id

  def initialize(id)
    @id = id
  end
end

RANGE = 1..10
TEST_RUNS = 10_000

arr = RANGE.map { |id| Record.new(id) }
# hash = RANGE.map { |id| [id, Record.new(id)] }.to_h

searchlist = Array.new(TEST_RUNS){ rand(RANGE.to_a.last)+1 }

array_speed = Benchmark.measure {
  sum = 0
  searchlist.each do |id|
    sum += arr.find { |record| record.id == id }.id
  end
  puts sum
}.real

hash_speed = Benchmark.measure {
  hash = arr.map { |record| [record.id, record] }.to_h
  sum = 0
  searchlist.each do |id|
    sum += hash[id].id
  end
  puts sum
}.real

puts "Array: #{array_speed}"
puts "Hash: #{hash_speed}"
puts "#{array_speed > hash_speed ? 'Hash' : 'Array'} was faster by #{array_speed > hash_speed ? array_speed / hash_speed : hash_speed / array_speed}"


puts " == Test 2 =="
Document = Struct.new(:id,:a,:b,:c)
documents_a = []
documents_h = {}
1.upto(10_000) do |n|
  d = Document.new(n, Random.new.rand(99..99999))
  documents_a << d
  # documents_h[d.id] = d
end
documents_h = 1.upto(10_000).map { |id| [id, Document.new(id, Random.new.rand(99..99999))] }.to_h

searchlist = Array.new(1000){ rand(10_000)+1 }

Benchmark.bm(10) do |x|
  x.report('array'){searchlist.each{|el| documents_a.find {|d| d.id == el}.a} }
  x.report('hash'){searchlist.each{|el| documents_h[el].a} }
end








