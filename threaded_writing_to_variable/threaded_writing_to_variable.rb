require 'benchmark'
require 'pry'
require 'concurrent'

COUNT = 100
REPEATED = 30

MUTEX = Mutex.new

class StandardMetricStore
  def initialize
    @state = 0
  end

  def increament
    MUTEX.synchronize {
      current_value = @state
      next_value = current_value * 100 / 100
      random_value = Random.new.rand(0..1000)
      next_value += random_value
      next_value -= random_value
      @state = (next_value + 1).to_i
    }
  end

  def count
    @state
  end
end

class AsyncMetricStore
  include Concurrent::Async

  def initialize
    @state = 0
  end

  def increament
    MUTEX.synchronize {
      current_value = @state
      next_value = current_value * 100 / 100
      random_value = Random.new.rand(0..1000)
      next_value += random_value
      next_value -= random_value
      @state = (next_value + 1).to_i
    }
  end

  def count
    @state
  end
end

class ChanelMetricStore
  include Concurrent::Async

  def initialize
    @channel = Concurrent::Channel.new
    @state = 0
  end

  def increament
    Concurrent::Channel.go do
      channel << 1
    end
  end

  def count
    @state
  end

  def start(threads_number)
    @threads = threads_number.times.map do
      Thread.new do
        loop do
          messages.take
          MUTEX.synchronize {
            current_value = @state
            next_value = current_value * 100 / 100
            random_value = Random.new.rand(0..1000)
            next_value += random_value
            next_value -= random_value
            @state = (next_value + 1).to_i
          }
        end
      end
    end
  end

  def stop
    @threads.each(&:kill)
  end
end

class QueuedMetricStore
  def initialize(finished)
    @finished = finished
    @state = 0
    @queue = Queue.new
  end

  def increament
    @queue.push 1
  end

  def start(threads_number)
    @threads = threads_number.times.map do
       Thread.new do
        loop do
          if @queue.size != 0
            @queue.pop
            MUTEX.synchronize {
              current_value = @state
              next_value = current_value * 100 / 100
              random_value = Random.new.rand(0..1000)
              next_value += random_value
              next_value -= random_value
              @state = (current_value + 1).to_i
            }
          end

          break if @finished[:finished] && @queue.size == 0
        end
      end
    end
  end

  def wait
    @threads.each(&:join)
  end

  def count
    @state
  end
end

def standard
  store = StandardMetricStore.new

  threads = []
  time = Benchmark.measure {
    threads = COUNT.times.map do
      Thread.new do
        COUNT.times do
          store.increament
        end
      end
    end
  }.real

  threads.each(&:join)
  raise StandardError, [store.count, COUNT * COUNT] if store.count != COUNT * COUNT

  time
end

def async
  store = AsyncMetricStore.new

  threads = []
  time = Benchmark.measure {
    threads = COUNT.times.map do
      Thread.new do
        COUNT.times do
          store.async.increament
        end
      end
    end
  }.real

  threads.each(&:join)
  while(store.count != COUNT * COUNT) do; end
  raise StandardError, [store.count, COUNT * COUNT] if store.count != COUNT * COUNT

  time
end

def channeled
  store = ChanelMetricStore.new
  store.start(20)

  threads = []
  time = Benchmark.measure {
    threads = COUNT.times.map do
      Thread.new do
        COUNT.times do
          store.async.increament
        end
      end
    end
  }.real

  threads.each(&:join)
  while(store.count != COUNT * COUNT) do; end
  raise StandardError, [store.count, COUNT * COUNT] if store.count != COUNT * COUNT
  store.stop

  time
end

# This is super slow due to context switches and threads pooling if something appeared in queue
def queued
  finished = { finished: false }
  store = QueuedMetricStore.new(finished)
  store.start(5)

  threads = []
  time = nil
  total = Benchmark.measure {
    time = Benchmark.measure {
      threads = COUNT.times.map do
        Thread.new do
          COUNT.times do
            store.increament
          end
        end
      end
      threads.each(&:join)
      finished[:finished] = true
    }.real
    store.wait
  }.real
  raise StandardError, [store.count, COUNT * COUNT] if store.count != COUNT * COUNT

  return time, total
end

puts "#{COUNT} threads, running #{COUNT} times, repeated #{REPEATED} times, avg time displayed:  "
puts 'Standard:  '
times = REPEATED.times.map { standard }
puts "In: #{times.sum / times.size}  "
puts "#{COUNT} threads, running #{COUNT} times, repeated #{REPEATED} times, avg time displayed:  "
puts 'Async:'
times = REPEATED.times.map { async }
puts "In: #{times.sum / times.size}s  "
puts 'Channeled (20 threads):  '
times = REPEATED.times.map { async }
puts "In: #{times.sum / times.size}s  "
#This is super slow due to context switches and threads pooling if something appeared in queue
puts 'Slow Queued:  '
times = REPEATED.times.map { queued }
time, total = times.transpose.map(&:sum)
puts "In: #{time / times.size}s  "
puts "Total (with queue emptying): #{total / times.size}s  "

