# Simple case about writing to variable from multiple threads
Imagine a situation where you have n threads, and you have to write to one variable from all the threads.  
Simple case could be metrics, where each thread processes some data and writes amount of processed entries to metric store that is shared among all threads.  
Then threads would block each other as they can't write simultaneously!   
But since these threads only increament value of this variable, maybe we could create a queue where we would store not processed increment requests and have separate thread for processing data from that queue?  
Or use async?
Or use channels?
## First things first
We can't use simplest integers as state as it's fast and (almost!) thread safe out of the box. Here I do some useless calculations with random number to simulate some load and make code thread unsafe - forcing using mutex and synchronization.
## Ruby 3.x
At the time of writing I couldn't install gem `concurrent-ruby` under 3.0.0 (compilation error)
# Results
`Queue was very slow, due to a lot of context switches - worker threads were pooling if something appeared in queue. Each new worker thread increased total time by significant amount where it should in theory shorten. No furtherer investigation.`  
### 100 threads, running 100 times, repeated 30 times, avg time displayed:  
Standard:  
In: 0.001056372500412787  
100 threads, running 100 times, repeated 30 times, avg time displayed:  
Async:
In: 0.0012092744271891811s  
Channeled (20 threads):  
In: 0.0010564808985994508s  
Slow Queued:  
In: 0.8663325819691333s  
Total (with queue emptying): 0.8911178821969467s
### 20 threads, running 20 times, repeated 30 times, avg time displayed:  
Standard:  
In: 0.0002319754004323234  
20 threads, running 20 times, repeated 30 times, avg time displayed:  
Async:
In: 0.0002354946006865551s  
Channeled (20 threads):  
In: 0.00024410686843718093s  
Slow Queued:  
In: 0.8595903754699975s  
Total (with queue emptying): 0.861337907796648
---
*all code here is written fast, take it into consideration, as knowing when write good code and when write code very fast is very usefull :)