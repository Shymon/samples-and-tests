package main

import (
	"fmt"
	"math/rand"
	"time"
)

// number of desired workers
const nWorkers = 10

func main() {
	rand.Seed(time.Now().UnixNano())

	// make a buffered channel with the space for my 10 workers
	workerChan := make(chan *worker, nWorkers)

	for i := 0; i < nWorkers; i++ {
		i := i
		wk := &worker{id: i}
		go wk.work(workerChan)
	}

	// read the channel, it will block until something is written, then a new
	// goroutine will start
	go func() {
		fmt.Println("[S] Watching ...")
		for wk := range workerChan {
			// log the error
			fmt.Printf("[S] Worker %d stopped with err: %s \n", wk.id, wk.err)
			// reset err
			wk.err = nil
			// a goroutine has ended, restart it
			fmt.Println("[S] Ressurecting ...")
			go wk.work(workerChan)
		}
	}()

	for {
		fmt.Println("[S] Doing some other stuff ...")
		select {
		case <-time.After(time.Second * (1)):
		}
	}
}

type worker struct {
	id  int
	err error
}

func (wk *worker) work(workerChan chan<- *worker) (err error) {
	// make my goroutine signal its death, wether it's a panic or a return
	fmt.Println("[W] Starting ...")
	defer func() {
		if r := recover(); r != nil {
			if err, ok := r.(error); ok {
				wk.err = err
			} else {
				wk.err = fmt.Errorf("[W] Panic happened with %v", r)
			}
		} else {
			wk.err = err
		}

		workerChan <- wk
	}()

	select {
	case <-time.After(time.Second * time.Duration(2+rand.Intn(8))):
	}
	fmt.Println("[W] Dying ...")
	panic("Gleba!")

	// return err
}
