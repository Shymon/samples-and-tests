package main

import (
	"net/http"
	"path/filepath"

	"github.com/gobuffalo/packr"
)

type spaHandler struct {
	box   *packr.Box
	index []byte
}

func applicationSpaHandler() spaHandler {
	box := packr.NewBox("./frontend/build")
	index, _ := box.Find("index.html")

	return spaHandler{box: &box, index: index}
}

func (h spaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if h.box.Has(path[1:]) {
		http.FileServer(h.box).ServeHTTP(w, r)
		return
	}

	w.Write(h.index)
}
