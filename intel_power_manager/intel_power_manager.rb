require 'pry'

class Watts
  attr_reader :count

  def initialize(count)
    raise ArgumentError, 'Invalid count' if count.nil? || count.zero?

    @count = count
  end

  def raw
    count * 1_000_000
  end

  def ==(other)
    count == other.count
  end
end

$environment = 'development'.freeze
# $environment = 'production'.freeze

def production?
  $environment == 'production'
end

class System
  class << self
    def read_memory_register(address)
      unless production?
        puts 'Non production environment'
        return 0
      end

      `devmem2 #{address} w`.split.last
    end

    def set_memory_register(address, value)
      unless production?
        puts "Setting memory register #{address} to #{value}"
        return true
      end

      `devmem2 #{address} w #{value}`
      true
    end

    def read_p1_power
      unless production?
        puts 'Non production environment'
        return Watts.new(999)
      end

      Watts.new(`cat /sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/constraint_0_power_limit_uw`)
    end

    def set_p1_power(power)
      raise 'Invalid power class' unless power.is_a?(Watts)

      unless production?
        puts "Setting p1 power to #{power.raw}"
        return true
      end

      `echo #{power.raw} | sudo tee /sys/devices/virtual/powercap/intel-rapl/intel-rapl:0/constraint_0_power_limit_uw`
      true
    end
  end
end

class Manipulator
  attr_reader :system, :target_p1_power
  private     :system, :target_p1_power

  POWER_LOCK_MEMORY_ADDRES_ONE = '0x00000000FED159A0'.freeze
  POWER_LOCK_MEMORY_ADDRES_TWO = '0x00000000FED159A4'.freeze

  POWER_LOCK_MEMORY_PROPER_VALUE_ONE = '0x00000000'.freeze
  POWER_LOCK_MEMORY_PROPER_VALUE_TWO = '0x80000000'.freeze

  def initialize(system:, target_p1_power:)
    @system = system
    raise ArgumentError, 'Invalid target_p1_power' unless target_p1_power.is_a?(Watts)

    @target_p1_power = target_p1_power
  end

  def ensure_power_lock
    results = []
    results << set_power_lock_memory_one unless power_lock_memory_one_valid?
    results << set_power_lock_memory_two unless power_lock_memory_two_valid?
    return if results.empty?

    results.all?
  end

  def ensure_p1_power
    set_p1_power unless p1_power_valid?
  end

  private

  def power_lock_memory_one_valid?
    Integer(system.read_memory_register(POWER_LOCK_MEMORY_ADDRES_ONE)) == Integer(POWER_LOCK_MEMORY_PROPER_VALUE_ONE)
  end

  def power_lock_memory_two_valid?
    Integer(system.read_memory_register(POWER_LOCK_MEMORY_ADDRES_TWO)) == Integer(POWER_LOCK_MEMORY_PROPER_VALUE_TWO)
  end

  def set_power_lock_memory_one
    system.set_memory_register(POWER_LOCK_MEMORY_ADDRES_ONE, POWER_LOCK_MEMORY_PROPER_VALUE_ONE)
  end

  def set_power_lock_memory_two
    system.set_memory_register(POWER_LOCK_MEMORY_ADDRES_TWO, POWER_LOCK_MEMORY_PROPER_VALUE_TWO)
  end

  def p1_power_valid?
    system.read_p1_power == target_p1_power
  end

  def set_p1_power
    system.set_p1_power(target_p1_power)
  end
end

class Runner
  class << self
    def run(target_p1_power:, interval: 5)
      manipulator = Manipulator.new(
        target_p1_power: target_p1_power,
        system: System
      )

      puts 'Starting...'
      puts 'Ensuring power lock'
      verify manipulator.ensure_power_lock

      Thread.new do
        loop do
          puts 'Enuring p1 power'
          verify manipulator.ensure_p1_power
          sleep(interval)
        end
      end

      loop { sleep(100) }
    end

    private

    def verify(command_result)
      return puts 'No need' if command_result.nil?
      return puts 'Changed' if command_result

      puts 'Failed' # if command_result == false
    end
  end
end

Runner.run(target_p1_power: Watts.new(20))
