package main

import (
	"encoding/binary"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/Shymon/samples-and-tests/go_modbus_rtu_via_tcp/modbus"
)

func main() {
	handler := modbus.NewRTUviaTCPClientHandler("localhost:10002")
	handler.Timeout = 1 * time.Second
	handler.SlaveId = 0xFE
	handler.Logger = log.New(os.Stdout, "test: ", log.LstdFlags)
	// Connect manually so that multiple requests are handled in one connection session
	err := handler.Connect()
	if err != nil {
		handler.Close()
		panic(err)
	}
	defer handler.Close()

	client := modbus.NewClient(handler)
	results, err := client.ReadHoldingRegisters(0, 4)
	fmt.Println(results)
	fmt.Println(binary.BigEndian.Uint16(results[2:4]))
}
