require 'rmodbus'

SERVER_PORT = 10_002

srv = ModBus::RTUViaTCPServer.new(SERVER_PORT, 254)
srv.coils = [1, 0, 1, 1]
srv.discrete_inputs = [1, 1, 0, 0]
srv.holding_registers = [1, 999, 3, 4]
srv.input_registers = [1, 2, 3, 4]
srv.debug = true

begin
  puts "Starting RTUViaTCPServer on port #{SERVER_PORT}"
  srv.start
  loop { sleep(100) }
rescue Interrupt
  puts 'Bye!'
  srv.shutdown
end
