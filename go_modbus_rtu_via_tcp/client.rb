require 'rmodbus'

ModBus::RTUViaTCPClient.connect('127.0.0.1', 10002) do |cl|
  cl.with_slave(255) do |slave|
    slave.debug = true
    regs = slave.holding_registers
    puts regs[0..3]
  end
end
