require 'singleton'
require 'securerandom'
require 'pry'

class Token
  attr_reader :token, :expires
  private :expires

  Threshold = 0.1

  def initialize
    @token = SecureRandom.hex(10)
    @expires = Time.now + 0.5
  end

  def expired?
    expires < Time.now + Threshold
  end
end

class TokenStore
  include Singleton

  def initialize
    register_token(:access_token, -> { Token.new })
    register_token(:user_token, -> { Token.new })
    register_token(:auth_token, -> { Token.new })
  end

  private

  def register_token(name, acquire_method)
    token = instance_variable_set("@#{name}", acquire_method.call)
    mutex = instance_variable_set("@#{name}_mutex", Mutex.new)

    self.class.send :define_method, name do 
      if token.expired? && !mutex.locked?
        mutex.synchronize do
          puts "#{Time.new.strftime("%T.%3N")}: Refreshing ..."
          sleep 0.5
          token = acquire_method.call
          token.token
        end
      else
        token.token
      end
    end
  end
end

puts TokenStore.instance

10.times do |t|
  Thread.new do |th|
    while true
      # puts "#{Time.new.strftime("%T.%3N")} #{t} #{TokenStore.instance.access_token}"
      puts "#{Time.new.strftime("%T.%3N")} #{t} #{TokenStore.instance.user_token}"
      # puts "#{Time.new.strftime("%T.%3N")} #{t} #{TokenStore.instance.auth_token}"
      sleep 0.05
    end
  end
end



sleep 5000