require 'rmodbus'

SERVER_PORT = 3015

srv = ModBus::TCPServer.new(SERVER_PORT, 255)
srv.holding_registers = [1]

begin
  puts 'Hello, I\'m PLC (for real)'
  puts "(Starting TCPServer on port #{SERVER_PORT})"
  srv.start
  loop {
    sleep((Random.new.rand * 100 + 50).to_i / 1000.0)

    srv.holding_registers = [Random.new.rand(1..50)] if (Random.new.rand * 10).to_i == 1
  }
rescue Interrupt
  puts 'Bye!'
  srv.shutdown
end
