module gitlab.com/Shymon/samples-and-tests/modbus-pooler

go 1.16

require (
	github.com/goburrow/modbus v0.1.0 // indirect
	github.com/goburrow/serial v0.1.0 // indirect
)
