require 'bundler/setup'
require 'hanami/api'
require 'hanami/middleware/body_parser'

use Hanami::Middleware::BodyParser, :json

class App < Hanami::API
  post '/notify' do
    puts params

    [200, 'Thanks!']
  end
end

puts 'Starting now ...'
run App.new
