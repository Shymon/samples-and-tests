package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/goburrow/modbus"
)

type Plc struct {
	name          string
	address       string
	notifications chan int
}

var notificationServerAddress = "http://localhost:3016/notify"
var plcs = []Plc{{"Plc1", "localhost:3015", make(chan int)}}
var ticker = time.NewTicker(100 * time.Millisecond)
var notificationTicker = time.NewTicker(1000 * time.Millisecond)
var done = make(chan bool)

func main() {
	startNotificationService()
	startPlcFetcherService()

	select {}
}

func startNotificationService() {
	go func() {
		for {
			select {
			case <-done:
				return
			case <-notificationTicker.C:
				payload := make(map[string]int)

				for _, plc := range plcs {
					select {
					case value := <-plc.notifications:
						payload[plc.name] = value
					default:
					}
				}

				if len(payload) != 0 {
					err := notify(payload)
					if err != nil {
						log.Println((err))
					}
				}
			}
		}
	}()
}

func notify(payload map[string]int) (err error) {
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	resp, err := http.Post(notificationServerAddress, "application/json", bytes.NewBuffer(jsonPayload))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

func startPlcFetcherService() {
	for _, _plc := range plcs {
		go func(plc Plc) {
			lastValue, err := fetchPlcValue(plc.address)
			if err != nil {
				lastValue = 0
				log.Println((err))
			}

			for {
				select {
				case <-ticker.C:
					newValue, err := fetchPlcValue(plc.address)
					if err != nil {
						newValue = lastValue
						log.Println((err))
					}
					if newValue != lastValue {
						plc.notifications <- newValue
						lastValue = newValue
					}
				}
			}
		}(_plc)
	}
}

func fetchPlcValue(plcAddress string) (value int, err error) {
	handler := modbus.NewTCPClientHandler(plcAddress)
	handler.Timeout = 1 * time.Second
	handler.SlaveId = 0xFF
	err = handler.Connect()
	if err != nil {
		return 0, err
	}
	defer handler.Close()

	client := modbus.NewClient(handler)
	results, err := client.ReadHoldingRegisters(0, 1)
	if err != nil {
		return 0, err
	}
	return int(binary.BigEndian.Uint16(results)), nil
}
