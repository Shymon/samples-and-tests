require_relative 'common'

WORKERS = ARGV[0]&.to_i || 5

pipe = Ractor.new do
  loop do
    Ractor.yield Ractor.recv
  end
end



workers = (1..WORKERS).map do |worker_id|
  Ractor.new(worker_id, pipe) do |worker_id, pipe|
    loop do
      job = pipe.take
      puts "Worker #{worker_id} performed: #{job}"
    end
  end
end

# Trap ^C
Signal.trap('INT') { exit }

# Trap `Kill `
Signal.trap('TERM') { exit }

loop do
  _, job = Redis.new.blpop(QUEUE)
  next if job.nil?

  pipe << JSON.parse(job)
end
