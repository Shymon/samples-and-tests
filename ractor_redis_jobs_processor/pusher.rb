require_relative 'common'

FROM = ARGV[0]&.to_i || 10
TO = ARGV[1]&.to_i || FROM + 10

Random.rand(FROM..TO).times do |n|
  REDIS.rpush(QUEUE, { msg: "Siema x#{n}" }.to_json)
end
