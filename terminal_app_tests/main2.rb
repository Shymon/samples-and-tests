require 'io/console'
require_relative 'colors'

question = 'Yes or no? (Y/n)'
user_status = 'Pepege'
enemy_status = 'Pogchamp'
by = 'Me 2k20'

def render(question, user_status, enemy_status, by)
  puts(question.ljust(50, ' ') + ('User status: ' + user_status).green)
  puts(''.rjust(50, ' ') + ('Enemy status: ' + enemy_status).red)
  puts('')
  puts(by.yellow)

  print("\u001b[3A")
end

render(question, user_status, enemy_status, by)

Thread.new do
  sleep(3)
  print("\u001b[1A")
  print("\u001b[100D")
  exit
end

loop do
  a = STDIN.getch
  print(a)
end
