require 'io/console'

def loading()
  puts 'Loading...'
  1.upto(100).each do |n|
    sleep(0.01)
    print("\u001b[1000D" + n.to_s + '%')
  end
  exit
end

Thread.new do
  loading
end

loop do
  STDIN.getch
  print("\u001b[1D")
end
