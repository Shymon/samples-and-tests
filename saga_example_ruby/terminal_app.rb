require 'io/console'
require_relative 'terminal_app/colors'
require_relative 'app/app'

class TerminalApp
  attr_reader :meta, :current_lines, :no_events, :events,
              :bus, :user, :product

  MIN_TERM_WIDTH = 125
  MIN_TERM_WIDTH_NO_EVENTS = 155
  LEFT_COLUMN_PADDING = 90
  MIDDLE_COLUMN_PADDING = 35

  CTRL_C = "\u0003".freeze

  PROCEED_ERROR = 'You have to confirm in order to proceed :) (press y)'.freeze

  def initialize(no_events: false)
    @no_events = no_events
    if IO.console.winsize[1] < MIN_TERM_WIDTH
      raise "Use with terminal of width at least #{MIN_TERM_WIDTH} \
            (with 'terminal-no-events' requires only #{MIN_TERM_WIDTH_NO_EVENTS})"
    end
    if no_events && IO.console.winsize[1] < MIN_TERM_WIDTH_NO_EVENTS
      raise "Use with terminal of width at least #{MIN_TERM_WIDTH_NO_EVENTS}"
    end

    @current_lines = []
    @bus = Bus.instance
    @user = Repos::UsersRepo.new.create
    @product = Repos::ProductsRepo.new.create
    @meta = {
      user: 'Current funds: '.yellow + user.funds.to_s.light_blue,
      discount: 'Current discounts: '.yellow + 'None'.red,
      order: 'Order: '.yellow + 'not dispatched'.red,
      payment: 'Payment status: '.yellow + 'none'.yellow
    }

    welcome_user
    subscribe_events
  end

  def run
    render 'Would you like to make an order?'
    if until_got_y?
      bus.dispatch(Orders::Commands::MakeOrder.new(name: 'Your order', product_id: product.id, user_id: user.id))
    end
    order = Repos::OrdersRepo.new.all.first

    render 'Is product from order availible? (affects next decision)'.red
    bus.dispatch(Products::Commands::MakeProductUnavailible.new(product_id: product.id)) unless got_y?

    render 'Would you like to confirm your order?'
    bus.dispatch(Orders::Commands::ConfirmOrder.new(order_id: order.id)) if until_got_y?

    render 'Would you like to make a payment?'
    bus.dispatch(Payments::Commands::StartPayment.new(order_id: order.id)) if until_got_y?
    payment = Repos::PaymentsRepo.new.all.first

    render 'Did payment fail?'
    if got_y?
      bus.dispatch(Payments::Commands::FailPayment.new(payment_id: payment.id))
      render 'Assuming you started next payment and it succeeded ... (wait ...)'
      sleep(0.5)
      bus.dispatch(Payments::Commands::StartPayment.new(order_id: order.id))
      payment = Repos::PaymentsRepo.new.all[1]
    end
    render 'Will product become unavailible AFTER payment is processed?'.red
    bus.dispatch(Products::Commands::MakeProductUnavailible.new(product_id: product.id)) if got_y?
    bus.dispatch(Payments::Commands::SucceedPayment.new(payment_id: payment.id))

    exit_program
  rescue Base::Exceptions::DomainException => e
    cursor_down(3)
    puts 'Domain exception occured!', e
    exit
  end

  private

  def got_y?
    char = $stdin.getch.downcase
    exit_program if ['q', CTRL_C].include?(char)
    char == 'y'
  end

  def until_got_y?
    loop do
      return true if got_y?

      render_error(PROCEED_ERROR)
    end
  end

  def exit_program
    cursor_down(3)
    puts 'Thanks for trying out program!'.pink
    exit
  end

  def rerender
    render(current_lines)
  end

  def render(lines)
    cursor_up(1)
    cursor_back(1000)

    @current_lines = [lines].flatten
    meta.each_with_index do |(_, value), i|
      current_line = current_lines[i].to_s
      # First column with text for user
      print current_line.cljust(LEFT_COLUMN_PADDING, ' ')
      erase_in_line
      # Second column with meta data
      print value.to_s.cljust(MIDDLE_COLUMN_PADDING, ' ')
      # Third column with events if so
      if no_events
        puts
      else
        puts events[-(meta.length - i)].to_s.cycle_color(events.length + i)
      end
    end

    cursor_up(meta.count - 1)
  end

  def render_error(error)
    error_lines = @current_lines
    error_lines[2] = error
    render(error_lines)
  end

  def cursor_down(times)
    print("\u001b[#{times}B")
  end

  def cursor_up(times)
    print("\u001b[#{times}A")
  end

  def cursor_back(times)
    print("\u001b[#{times}D")
  end

  def erase_in_line
    print "\u001b[0K"
  end

  def welcome_user
    puts 'Saga example by Shymon (Szymon Ruta) gitlab.com/shymon'.pink
    puts 'Follow instructions to try out flow, answer: '.pink + 'y'.green + ' / '.pink + 'n'.red + '. Use '.pink +
         'q'.yellow + ' or '.pink + 'ctrl-c'.yellow + ' to quit.'.pink
    puts
  end

  def subscribe_events
    @events = []
    bus.subscribe_all(proc { |event| events << event.class }) unless no_events

    bus.register(Orders::Events::OrderDispatched, proc {
      meta[:order] = 'Order: '.yellow + 'successfuly dispatched :)'.green
      render 'Order successfuly dispatched :)'.green
    })

    bus.register(Payments::Events::PaymentStarted, proc {
      meta[:payment] = 'Payment status: '.yellow + 'started'.blue
      rerender
    })

    bus.register(Payments::Events::PaymentFailed, proc {
      meta[:payment] = 'Payment status: '.yellow + 'failed'.red
      rerender
    })

    bus.register(Payments::Events::PaymentSucceeded, proc {
      meta[:payment] = 'Payment status: '.yellow + 'succeded'.light_blue
      rerender
    })

    bus.register(Payments::Events::PaymentFinished, proc {
      meta[:payment] = 'Payment status: '.yellow + 'finished'.green
      rerender
    })

    bus.register(Payments::Events::PaymentReverted, proc {
      meta[:payment] = 'Payment status: '.yellow + 'reverted'.pink
      rerender
      render 'Your funds were returnd!'.yellow
    })

    bus.register(Users::Events::FundsCharged, proc { |event|
      meta[:user] = 'Current funds: '.yellow + event.current_balance.to_s.red
      rerender
    })

    bus.register(Users::Events::FundsReturned, proc { |event|
      meta[:user] = 'Current funds: '.yellow + event.current_balance.to_s.green
      rerender
    })

    bus.register(Users::Events::DiscountProposed, proc {
      render 'Since product is unavailible, do you want some discount?'
      if got_y?
        bus.dispatch(Users::Commands::AcceptDiscount.new(order_id: Repos::OrdersRepo.new.all.first.id))
      else
        bus.dispatch(Users::Commands::RejectDiscount.new(order_id: Repos::OrdersRepo.new.all.first.id))
      end
    })

    bus.register(Users::Events::DiscountGranted, proc { |event|
      return unless event.user_id == user.id

      meta[:discount] = 'Current discounts: '.yellow + event.value.to_s.green + '%'.green
      rerender
      puts 'You got discout for next purchase!'.green
    })
  end
end
