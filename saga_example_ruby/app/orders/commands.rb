require 'base'

module Orders
  module Commands
    class MakeOrder
      include Base::Command

      option :name
      option :user_id
      option :product_id
    end

    class ConfirmOrder
      include Base::Command

      option :order_id
    end

    class DispatchOrder
      include Base::Command

      option :order_id
    end
  end
end
