require 'base'
require 'repos'

require 'payments/events'
require 'payments/commands'
require 'orders/commands'
require 'orders/events'
require 'users/commands'
require 'users/events'

module Orders
  module Sagas
    class Dispatching
      include Base::Saga

      attr_reader :payments_repo

      on Payments::Events::PaymentFinished,
         dispatch: Orders::Commands::DispatchOrder,
         map: ->(payment_finished, saga) {
           order = saga.payments_repo.find(payment_finished.payment_id).order
           { order_id: order.id }
         }

      def initialize(payments_repo: Repos::PaymentsRepo.new)
        @payments_repo = payments_repo
      end
    end

    class Discounts
      include Base::Saga

      attr_reader :orders_repo

      on Orders::Events::OrderDispatchError,
         dispatch: Users::Commands::ProposeDiscount,
         map: ->(order_dispatch_error, _) { { order_id: order_dispatch_error.order_id } }

      on Users::Events::DiscountRejected,
         dispatch: Payments::Commands::RevertPayment,
         map: ->(discount_rejected, saga) {
           order = saga.orders_repo.find(discount_rejected.order_id)
           { payment_id: order.succeded_payment.id }
         }

      def initialize(orders_repo: Repos::OrdersRepo.new)
        @orders_repo = orders_repo
      end
    end
  end
end
