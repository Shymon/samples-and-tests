require 'dry-initializer'

require 'repos'
require 'bus'
require 'base'
require 'orders/exceptions'
require 'orders/events'

module Orders
  module Handlers
    class MakeOrderHandler
      include Base::Handler

      option :orders_repo, default: proc { Repos::OrdersRepo.new }

      def call(make_order_command)
        order = orders_repo.create(**make_order_command.all_data)
        Bus.instance.dispatch(Orders::Events::OrderMade.new(order_id: order.id))
      end
    end

    class ConfirmOrderHandler
      include Base::Handler

      option :orders_repo, default: proc { Repos::OrdersRepo.new }

      def call(command)
        order = intelli_load(:order, command.order_id)
        raise Orders::Exceptions::ProductNotAvailible unless order.product.availible

        orders_repo.save(order.id, confirmed: true)
        true
      end
    end

    class DispatchOrderHandler
      include Base::Handler

      option :orders_repo, default: proc { Repos::OrdersRepo.new }

      def call(command)
        order = intelli_load(:order, command.order_id)
        order.dispatch

        if order.product.availible
          success = orders_repo.save(order.id, order)
          Bus.instance.dispatch(Orders::Events::OrderDispatched.new(order_id: order.id)) if success
        else
          Bus.instance.dispatch(Orders::Events::OrderDispatchError.new(order_id: order.id))
        end
        true
      end
    end
  end
end
