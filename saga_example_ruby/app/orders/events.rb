require 'base'

module Orders
  module Events
    class OrderMade
      include Base::Event

      option :order_id
    end

    class OrderDispatched
      include Base::Event

      option :order_id
    end

    class OrderDispatchError
      include Base::Event

      option :order_id
    end
  end
end
