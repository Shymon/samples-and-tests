require 'base'

module Orders
  module Models
    class Order
      include Base::Model

      DEFAULT_PRICE = 15

      option :name, optional: true
      option :confirmed, default: proc { false }
      option :dispatched, default: proc { false }
      option :user
      option :product
      option :price, default: proc { DEFAULT_PRICE } # TODO: that should be in product :P

      def dispatch
        raise Exceptions::Orders::NotPaid unless succeded_payment

        @dispatched = true
      end

      def succeded_payment
        Repos::PaymentsRepo.new.for_order(self).find { |p| p.finished && !p.failed }
      end
    end
  end
end
