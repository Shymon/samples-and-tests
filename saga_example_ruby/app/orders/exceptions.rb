require 'base'

module Orders
  module Exceptions
    class ProductNotAvailible < Base::Exceptions::DomainException; end
    class NotPaid             < Base::Exceptions::DomainException; end
  end
end
