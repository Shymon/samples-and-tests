require 'base'

require 'payments/commands'
require 'payments/events'
require 'users/events'
require 'users/commands'

module Payments
  module Sagas
    class Charging
      include Base::Saga

      on Users::Events::FundsCharged,
         dispatch: Payments::Commands::FinishPayment,
         map: ->(funds_charged, _) { { payment_id: funds_charged.payment_id } }

      on Payments::Events::PaymentSucceeded,
         dispatch: Users::Commands::ChargeFunds,
         map: ->(event, _) { { payment_id: event.payment_id } }

      on Payments::Events::PaymentReverted,
         dispatch: Users::Commands::ReturnFunds,
         map: ->(event, _) { { payment_id: event.payment_id } }
    end
  end
end
