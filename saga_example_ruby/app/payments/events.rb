require 'base'

module Payments
  module Events
    class PaymentStarted
      include Base::Event

      option :payment_id
    end

    class PaymentSucceeded
      include Base::Event

      option :payment_id
    end

    class PaymentFailed
      include Base::Event

      option :payment_id
    end

    class PaymentFinished
      include Base::Event

      option :payment_id
    end

    class PaymentReverted
      include Base::Event

      option :payment_id
    end
  end
end
