require 'base'

module Payments
  module Models
    class Payment
      include Base::Model

      option :failed, default: proc { false }
      option :finished, default: proc { false }
      option :reverted, default: proc { false }
      option :order

      def fail
        raise Exceptions::Payments::AlreadyFinished if finished

        @failed = true
        @finished = true
      end

      def finish
        raise Exceptions::Payments::AlreadyFinished if finished

        @finished = true
      end

      def revert
        raise Exceptions::Payments::CannotRevertFailed if failed
        raise Exceptions::Payments::CannotRevertNotFinished unless finished

        @reverted = true
      end

      def user
        order.user
      end

      def amount
        order.price
      end
    end
  end
end
