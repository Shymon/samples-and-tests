require 'base'

module Payments
  module Commands
    class StartPayment
      include Base::Command

      option :order_id
    end

    class FailPayment
      include Base::Command

      option :payment_id
    end

    class SucceedPayment
      include Base::Command

      option :payment_id
    end

    class FinishPayment
      include Base::Command

      option :payment_id
    end

    class RevertPayment
      include Base::Command

      option :payment_id
    end
  end
end
