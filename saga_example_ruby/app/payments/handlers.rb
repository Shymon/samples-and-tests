require 'dry-initializer'

require 'repos'
require 'bus'
require 'base'
require 'payments/exceptions'
require 'payments/events'

module Payments
  module Handlers
    class StartPaymentHandler
      include Base::Handler

      option :payments_repo, default: proc { Repos::PaymentsRepo.new }

      def call(command)
        order = intelli_load(:order, command.order_id)
        payment = payments_repo.create(order: order)
        Bus.instance.dispatch(Events::PaymentStarted.new(payment_id: payment.id)) if payment
        true
      end
    end

    class FailPaymentHandler
      include Base::Handler

      option :payments_repo, default: proc { Repos::PaymentsRepo.new }

      def call(command)
        payment = intelli_load(:payment, command.payment_id)
        payment.fail
        success = payments_repo.save(payment.id, payment)
        Bus.instance.dispatch(Events::PaymentFailed.new(payment_id: payment.id)) if success
        true
      end
    end

    class SucceedPaymentHandler
      include Base::Handler

      def call(command)
        Bus.instance.dispatch(Events::PaymentSucceeded.new(payment_id: command.payment_id))
        true
      end
    end

    class FinishPaymentHandler
      include Base::Handler

      option :payments_repo, default: proc { Repos::PaymentsRepo.new }

      def call(command)
        payment = intelli_load(:payment, command.payment_id)
        payment.finish
        success = payments_repo.save(payment.id, payment)
        Bus.instance.dispatch(Events::PaymentFinished.new(payment_id: payment.id)) if success
        true
      end
    end

    class RevertPaymentHandler
      include Base::Handler

      option :payments_repo, default: proc { Repos::PaymentsRepo.new }

      def call(command)
        payment = intelli_load(:payment, command.payment_id)
        payment.revert
        success = payments_repo.save(payment.id, payment)
        Bus.instance.dispatch(Events::PaymentReverted.new(payment_id: payment.id)) if success
        true
      end
    end
  end
end
