require 'base'

module Payments
  module Exceptions
    class AlreadyFinished           < Base::Exceptions::DomainException; end
    class CannotRevertFailed        < Base::Exceptions::DomainException; end
    class CannotRevertNotFinished   < Base::Exceptions::DomainException; end
  end
end
