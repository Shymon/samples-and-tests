require 'base'

module Users
  module Models
    class User
      include Base::Model

      DEFAULT_FUNDS = 100

      option :funds, default: -> { DEFAULT_FUNDS }
      option :discount, default: -> { 0 }

      def give_discount(discount)
        raise Exceptions::Users::InvalidDiscount if discount.negative? || discount > 100

        @discount = discount
      end

      def charge_funds(amount)
        raise Exceptions::Users::InvalidFundsAmount, amount if amount.negative?
        raise Exceptions::Users::TooPoor if funds < amount

        @funds -= amount
      end

      def return_funds(amount)
        raise Exceptions::Users::InvalidFundsAmount, amount if amount.negative?

        @funds += amount
      end
    end
  end
end
