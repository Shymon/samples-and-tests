require 'base'

module Users
  module Events
    class DiscountProposed
      include Base::Event

      option :order_id
    end

    class DiscountRejected
      include Base::Event

      option :order_id
    end

    class DiscountGranted
      include Base::Event

      option :user_id
      option :order_id
      option :value
    end

    class FundsCharged
      include Base::Event

      option :payment_id
      option :current_balance
    end

    class FundsReturned
      include Base::Event

      option :payment_id
      option :current_balance
    end
  end
end
