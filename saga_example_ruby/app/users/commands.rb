require 'base'

module Users
  module Commands
    class ProposeDiscount
      include Base::Command

      option :order_id
    end

    class RejectDiscount
      include Base::Command

      option :order_id
    end

    class AcceptDiscount
      include Base::Command

      option :order_id
    end

    class ChargeFunds
      include Base::Command

      option :payment_id
    end

    class ReturnFunds
      include Base::Command

      option :payment_id
    end
  end
end
