require 'base'

module Users
  module Exceptions
    class InvalidDiscount     < Base::Exceptions::DomainException; end
    class InvalidFundsAmount  < Base::Exceptions::DomainException; end
    class TooPoor             < Base::Exceptions::DomainException; end
  end
end
