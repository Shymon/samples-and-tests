require 'dry-initializer'

require 'repos'
require 'bus'
require 'base'
require 'users/exceptions'
require 'users/events'

module Users
  module Handlers
    class ProposeDiscountHandler
      include Base::Handler

      def call(command)
        Bus.instance.dispatch(Events::DiscountProposed.new(order_id: command.order_id))
      end
    end

    class AcceptDiscountHandler
      include Base::Handler

      DISCOUNT_VALUE = 10

      option :users_repo, default: proc { Repos::UsersRepo.new }

      def call(command)
        user = intelli_load(:order, command.order_id).user
        user.give_discount(DISCOUNT_VALUE) # Very simple logic ikr :)
        return unless users_repo.save(user.id, user)

        Bus.instance.dispatch(
          Events::DiscountGranted.new(
            order_id: command.order_id,
            user_id: user.id,
            value: DISCOUNT_VALUE
          )
        )
      end
    end

    class RejectDiscountHandler
      include Base::Handler

      def call(command)
        Bus.instance.dispatch(Events::DiscountRejected.new(order_id: command.order_id))
      end
    end

    class ChargeFundsHandler
      include Base::Handler

      DISCOUNT_VALUE = 10

      option :payments_repo, default: proc { Repos::PaymentsRepo.new }
      option :users_repo, default: proc { Repos::UsersRepo.new }

      def call(command)
        payment = intelli_load(:payment, command.payment_id)
        user = payment.user
        user.charge_funds(payment.amount) # Very simple logic ikr :)
        charge_success = users_repo.save(user.id, user)
        return unless charge_success

        Bus.instance.dispatch(Events::FundsCharged.new(payment_id: payment.id, current_balance: user.funds))
      end
    end

    class ReturnFundsHandler
      include Base::Handler

      DISCOUNT_VALUE = 10

      option :payments_repo, default: proc { Repos::PaymentsRepo.new }
      option :users_repo, default: proc { Repos::UsersRepo.new }

      def call(command)
        payment = intelli_load(:payment, command.payment_id)
        user = payment.user
        user.return_funds(payment.amount) # Very simple logic ikr :)
        success = users_repo.save(user.id, user)
        Bus.instance.dispatch(Events::FundsReturned.new(payment_id: payment.id, current_balance: user.funds)) if success
      end
    end
  end
end
