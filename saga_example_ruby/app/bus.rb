require 'singleton'

class Bus
  attr_reader :handlers_registry
  private     :handlers_registry

  include Singleton

  def register(message, handler)
    @handlers_registry ||= {}
    @handlers_registry[message.name] ||= []
    @handlers_registry[message.name] << handler
  end

  def register_many(messages_and_hanlders)
    messages_and_hanlders.each do |message, handler|
      register(message, handler)
    end
  end

  def dispatch(message)
    global_listeners.each { |gl| gl.call(message) }
    (@handlers_registry[message.class.name] || []).each do |handler|
      queue << [handler, message]
    end

    while queue.any?
      current_handler, current_message = queue.shift
      return if current_handler.nil?

      # puts "Dispatching #{current_message.class}"
      current_handler.is_a?(Class) ? current_handler.new.call(current_message) : current_handler.call(current_message)
    end
  end

  def clean
    @handlers_registry = {}
    @global_listeners = []
  end

  def subscribe_all(proc_handler)
    global_listeners << proc_handler
  end

  private

  def queue
    @queue ||= []
  end

  def global_listeners
    @global_listeners ||= []
  end
end
