require 'bundler'
Bundler.require(:default)

require 'orders/handlers'
require 'orders/commands'
require 'orders/sagas'

require 'products/handlers'
require 'products/commands'

require 'users/handlers'
require 'users/commands'

require 'payments/handlers'
require 'payments/commands'
require 'payments/sagas'


Bus.instance.register_many(
  [
    # Orders
    [
      Orders::Commands::MakeOrder,
      Orders::Handlers::MakeOrderHandler
    ],
    [
      Orders::Commands::ConfirmOrder,
      Orders::Handlers::ConfirmOrderHandler
    ],
    [
      Orders::Commands::DispatchOrder,
      Orders::Handlers::DispatchOrderHandler
    ],
    # Products
    [
      Products::Commands::MakeProductUnavailible,
      Products::Handlers::MakeProductUnavailibleHandler
    ],
    # Payments
    [
      Payments::Commands::StartPayment,
      Payments::Handlers::StartPaymentHandler
    ],
    [
      Payments::Commands::FailPayment,
      Payments::Handlers::FailPaymentHandler
    ],
    [
      Payments::Commands::SucceedPayment,
      Payments::Handlers::SucceedPaymentHandler
    ],
    [
      Payments::Commands::FinishPayment,
      Payments::Handlers::FinishPaymentHandler
    ],
    [
      Payments::Commands::RevertPayment,
      Payments::Handlers::RevertPaymentHandler
    ],
    # Users
    [
      Users::Commands::ProposeDiscount,
      Users::Handlers::ProposeDiscountHandler
    ],
    [
      Users::Commands::AcceptDiscount,
      Users::Handlers::AcceptDiscountHandler
    ],
    [
      Users::Commands::RejectDiscount,
      Users::Handlers::RejectDiscountHandler
    ],
    [
      Users::Commands::ChargeFunds,
      Users::Handlers::ChargeFundsHandler
    ],
    [
      Users::Commands::ReturnFunds,
      Users::Handlers::ReturnFundsHandler
    ]
  ]
)

Payments::Sagas::Charging.new.start
Orders::Sagas::Discounts.new.start
Orders::Sagas::Dispatching.new.start
