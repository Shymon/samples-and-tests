require 'base'

module Products
  module Models
    class Product
      include Base::Model

      option :availible, default: proc { true }
    end
  end
end
