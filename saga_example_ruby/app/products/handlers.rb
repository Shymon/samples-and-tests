require 'dry-initializer'

require 'repos'
require 'bus'
require 'base'
require 'products/exceptions'
require 'products/events'

module Products
  module Handlers
    class MakeProductUnavailibleHandler
      include Base::Handler

      option :products_repo, default: proc { Repos::ProductsRepo.new }

      def call(command)
        products_repo.save(command.product_id, availible: false)
        true
      end
    end
  end
end
