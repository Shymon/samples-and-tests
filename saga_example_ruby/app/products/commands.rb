require 'base'

module Products
  module Commands
    class MakeProductUnavailible
      include Base::Command

      option :product_id
    end
  end
end
