require 'singleton'

class Database
  include Singleton

  def clean
    instance_variables.each do |instance_variable|
      instance_variable_set instance_variable, {}
    end
  end

  def insert(table, data)
    variable = instance_variable_get "@#{table}"
    variable ||= {}
    variable[data[:id]] = data
    instance_variable_set "@#{table}", variable
    true
  end

  def select_by_id(table, id)
    variable = instance_variable_get "@#{table}"
    variable ||= {}
    variable[id]
  end

  def all(table)
    variable = instance_variable_get "@#{table}"
    variable ||= {}
    variable.values
  end

  def update_by_id(table, id, attributes)
    variable = instance_variable_get "@#{table}"
    variable ||= {}
    variable[id] = variable[id].merge(attributes)
    instance_variable_set "@#{table}", variable
    true
  end
end
