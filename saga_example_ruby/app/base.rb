require 'securerandom'
require 'dry-initializer'

require 'bus'

module Base
  module Command
    def self.included(base)
      base.extend Dry::Initializer
    end

    def all_data
      self.class.dry_initializer.attributes(self)
    end
  end

  module Event
    def self.included(base)
      base.extend Dry::Initializer
    end
  end

  module Exceptions
    class DomainException < StandardError; end
  end

  module Handler
    def self.included(base)
      base.extend Dry::Initializer
    end

    def intelli_load(model_name, model_id)
      Object.const_get("Repos::#{model_name.to_s.capitalize}sRepo").new.find(model_id)
    end
  end

  module Model
    def self.included(base)
      base.extend Dry::Initializer
      base.extend ClassMethods

      base.option :id
    end

    module ClassMethods
      def build(attributes)
        new(**attributes.merge(id: generate_id))
      end

      def generate_id
        SecureRandom.uuid
      end
    end
  end

  module Saga
    def self.included(base)
      base.extend ClassMethods
    end

    module ClassMethods
      attr_reader :saga_processes

      def on(message, dispatch:, map: ->(_, _) { {} })
        @saga_processes ||= []
        @saga_processes.push(
          {
            message: message,
            next_command: dispatch,
            map_method: map
          }
        )
      end
    end

    def start
      self.class.saga_processes.each do |data|
        Bus.instance.register(
          data[:message],
          ->(command) { Bus.instance.dispatch(data[:next_command].new(**data[:map_method].call(command, self))) }
        )
      end
    end
  end
end
