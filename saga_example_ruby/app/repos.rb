require 'singleton'
require 'database'

require 'orders/models/order'
require 'payments/models/payment'
require 'products/models/product'
require 'users/models/user'

module Repos
  module BaseRepo
    def self.included(base)
      base.extend ClassMethods
      base.init
    end

    attr_reader :database
    private     :database

    module ClassMethods
      attr_accessor :model_class, :table_name, :associations

      def init
        @associations = []
      end

      def set_model_class(klass)
        @model_class = klass
        @table_name = "#{klass.name.downcase.split('::').last}s" # no pluralize in pure ruby :(
      end

      def association(klass)
        name = klass.name.split('::').last.downcase.to_sym

        define_method "get_#{name}" do |attributes|
          Object.const_get("Repos::#{name.capitalize}sRepo").new.find(attributes[:"#{name}_id"])
        end
        @associations += [name]
      end
    end

    def model_class
      self.class.model_class
    end

    def table_name
      self.class.table_name
    end

    def initialize(database = Database.instance)
      @database = database
    end

    def create(attributes = {})
      object = model_class.build(load_associations(attributes))
      success = @database.insert(table_name, attributes_for(object))
      success && object
    end

    def find(id)
      attributes = attributes_from(@database.select_by_id(table_name, id))
      raise("Could not find #{model_class} with id #{id}") unless attributes

      model_class.new(**load_associations(attributes))
    end

    def all
      @database.all(table_name).map do |attrs|
        model_class.new(**load_associations(attributes_from(attrs)))
      end
    end

    def save(id, attributes_or_object)
      if attributes_or_object.is_a?(Hash)
        @database.update_by_id(table_name, id, attributes_or_object)
      else
        @database.update_by_id(table_name, id, attributes_for(attributes_or_object))
      end
    end

    def reload(object)
      find(object.id)
    end

    private

    def load_associations(attributes)
      self.class.associations.each do |association|
        attributes[association] = send("get_#{association}", attributes) unless attributes[association]
        attributes[:"#{association}_id"] = attributes[association].id unless attributes[:"#{association}_id"]
      end
      attributes
    end

    def attributes_from(database_attrs)
      database_attrs.dup
    end
  end

  class UsersRepo
    include BaseRepo

    set_model_class Users::Models::User

    def attributes_for(user)
      { id: user.id, funds: user.funds, discount: user.discount }
    end
  end

  class OrdersRepo
    include BaseRepo

    association Users::Models::User
    association Products::Models::Product

    set_model_class Orders::Models::Order

    def attributes_for(order)
      {
        id: order.id,
        name: order.name,
        confirmed: order.confirmed,
        dispatched: order.dispatched,
        user_id: order.user.id,
        product_id: order.product.id,
        price: order.price
      }
    end
  end

  class PaymentsRepo
    include BaseRepo

    association Orders::Models::Order

    set_model_class Payments::Models::Payment

    def attributes_for(payment)
      {
        id: payment.id,
        order_id: payment.order.id,
        failed: payment.failed,
        finished: payment.finished,
        reverted: payment.reverted
      }
    end

    def for_order(order)
      all.select { |payment| payment.order.id == order.id }
    end
  end

  class ProductsRepo
    include BaseRepo

    set_model_class Products::Models::Product

    def attributes_for(product)
      { id: product.id, availible: product.availible }
    end
  end
end
