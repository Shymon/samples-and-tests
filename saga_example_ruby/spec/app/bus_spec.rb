require 'spec_helper'

require 'bus'
require 'repos'
require 'orders/commands'

RSpec.describe Bus do
  let(:user) { Repos::UsersRepo.new.create }
  let(:bus) { Bus.instance }
  let(:product) { Repos::ProductsRepo.new.create }

  after { bus.clean }

  it 'calls handler for event' do
    handler = double
    bus.register(Orders::Commands::MakeOrder, handler)

    expect(handler).to receive(:call)
    bus.dispatch(Orders::Commands::MakeOrder.new(name: 'asdf', user_id: user.id, product_id: product.id))
  end

  it 'calls multiple handlers for events' do
    handler = double
    handler2 = double
    bus.register_many(
      [
        [Orders::Commands::MakeOrder, handler],
        [Orders::Commands::MakeOrder, handler2]
      ]
    )

    expect(handler).to receive(:call)
    expect(handler2).to receive(:call)
    bus.dispatch(Orders::Commands::MakeOrder.new(name: 'asdf', user_id: user.id, product_id: product.id))
  end

  it 'calls global listener for every events' do
    handler = double(:global_listener)
    bus.subscribe_all(handler)

    RandomEvent = Class.new
    RandomEvent2 = Class.new
    bus.register_many(
      [
        [RandomEvent, proc {}],
        [RandomEvent2, proc {}]
      ]
    )
    expect(handler).to receive(:call).twice
    bus.dispatch(RandomEvent.new)
    bus.dispatch(RandomEvent2.new)
  end
end
