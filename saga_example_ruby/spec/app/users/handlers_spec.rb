require 'spec_helper'

require 'repos'
require 'users/commands'
require 'users/events'
require 'users/exceptions'
require 'users/handlers'

RSpec.describe 'Users handlers' do
  let(:user) { Repos::UsersRepo.new.create }
  let(:product) { Repos::ProductsRepo.new.create }
  let(:bus) { Bus.instance }

  after { bus.clean }
  after { Database.instance.clean }

  describe Users::Handlers::AcceptDiscountHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let!(:payment) { Repos::PaymentsRepo.new.create(order: order, finished: true) }

    it 'gives user discount' do
      bus.register(Users::Commands::AcceptDiscount, Users::Handlers::AcceptDiscountHandler)

      expect { bus.dispatch(Users::Commands::AcceptDiscount.new(order_id: order.id)) }.to(
        change { Repos::UsersRepo.new.reload(user).discount }.from(0).to(Users::Handlers::AcceptDiscountHandler::DISCOUNT_VALUE)
      )
    end

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Users::Events::DiscountGranted,
      Users::Commands::AcceptDiscount,
      Users::Handlers::AcceptDiscountHandler
    ) do
      let(:command_params) { { order_id: order.id } }
    end
  end

  describe Users::Handlers::ProposeDiscountHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let!(:payment) { Repos::PaymentsRepo.new.create(order: order, finished: true) }

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Users::Events::DiscountProposed,
      Users::Commands::ProposeDiscount,
      Users::Handlers::ProposeDiscountHandler
    ) do
      let(:command_params) { { order_id: order.id } }
    end
  end

  describe Users::Handlers::RejectDiscountHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let!(:payment) { Repos::PaymentsRepo.new.create(order: order, finished: true) }

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Users::Events::DiscountRejected,
      Users::Commands::RejectDiscount,
      Users::Handlers::RejectDiscountHandler
    ) do
      let(:command_params) { { order_id: order.id } }
    end
  end

  describe Users::Handlers::ChargeFundsHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let!(:payment) { Repos::PaymentsRepo.new.create(order: order, finished: true) }

    it 'gives user discount' do
      bus.register(Users::Commands::ChargeFunds, Users::Handlers::ChargeFundsHandler)

      expect { bus.dispatch(Users::Commands::ChargeFunds.new(payment_id: payment.id)) }.to(
        change { Repos::UsersRepo.new.reload(user).funds }.from(user.funds).to(user.funds - payment.amount)
      )
    end

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Users::Events::FundsCharged,
      Users::Commands::ChargeFunds,
      Users::Handlers::ChargeFundsHandler
    ) do
      let(:command_params) { { payment_id: payment.id } }
    end
  end

  describe Users::Handlers::ReturnFundsHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let!(:payment) { Repos::PaymentsRepo.new.create(order: order, finished: true) }

    it 'gives user discount' do
      bus.register(Users::Commands::ReturnFunds, Users::Handlers::ReturnFundsHandler)

      expect { bus.dispatch(Users::Commands::ReturnFunds.new(payment_id: payment.id)) }.to(
        change { Repos::UsersRepo.new.reload(user).funds }.from(user.funds).to(user.funds + payment.amount)
      )
    end

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Users::Events::FundsReturned,
      Users::Commands::ReturnFunds,
      Users::Handlers::ReturnFundsHandler
    ) do
      let(:command_params) { { payment_id: payment.id } }
    end
  end
end
