require 'spec_helper'

require 'repos'
require 'products/commands'
require 'products/events'
require 'products/exceptions'
require 'products/handlers'

RSpec.describe 'Products handlers' do
  let(:user) { Repos::UsersRepo.new.create }
  let(:product) { Repos::ProductsRepo.new.create }
  let(:bus) { Bus.instance }

  after { bus.clean }
  after { Database.instance.clean }

  describe Products::Handlers::MakeProductUnavailibleHandler do
    it 'makes product unavailible' do
      product
      bus.register(Products::Commands::MakeProductUnavailible, Products::Handlers::MakeProductUnavailibleHandler)

      expect { bus.dispatch(Products::Commands::MakeProductUnavailible.new(product_id: product.id)) }.to(
        change { Repos::ProductsRepo.new.all.first.availible }.from(true).to(false)
      )
    end
  end
end
