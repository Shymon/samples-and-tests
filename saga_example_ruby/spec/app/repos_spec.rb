require 'spec_helper'

require 'repos'
require 'orders/models/order'
require 'users/models/user'
require 'payments/models/payment'

RSpec.describe Repos do
  it 'creates user' do
    expect(Repos::UsersRepo.new.create).to be_a(Users::Models::User)
    expect(Repos::UsersRepo.new.all.count).to eq(1)
  end

  it 'creates order' do
    # Nice to have factory here but it's too simple and out of scope for this project
    user = Repos::UsersRepo.new.create
    product = Repos::ProductsRepo.new.create
    expect(Repos::OrdersRepo.new.create(name: 'foo', user: user, product: product)).to be_a(Orders::Models::Order).and have_attributes(name: 'foo')
    expect(Repos::OrdersRepo.new.all.count).to eq(1)
  end

  it 'creates payment' do
    user = Repos::UsersRepo.new.create
    product = Repos::ProductsRepo.new.create
    order = Repos::OrdersRepo.new.create(name: 'foo', user: user, product: product)
    expect(Repos::PaymentsRepo.new.create(order: order)).to be_a(Payments::Models::Payment)
    expect(Repos::PaymentsRepo.new.all.count).to eq(1)
  end
end
