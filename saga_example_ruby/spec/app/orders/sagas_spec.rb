require 'spec_helper'

require 'orders/sagas'

RSpec.describe 'Orders sagas' do
  let(:bus) { Bus.instance }
  let(:user) { Repos::UsersRepo.new.create }
  let(:product) { Repos::ProductsRepo.new.create(availible: false) }
  let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
  let!(:payment) { Repos::PaymentsRepo.new.create(order: order) }
  let(:saga) { described_class.new }

  after { bus.clean }
  after { Database.instance.clean }

  describe Orders::Sagas::Dispatching do
    context 'after PaymentFinished, dispatches DispatchOrder' do
      it_behaves_like(
        'saga dispatches command after receiving event',
        Orders::Commands::DispatchOrder,
        Payments::Events::PaymentFinished
      ) do
        let(:event_params) { { payment_id: payment.id } }
      end
    end
  end

  describe Orders::Sagas::Discounts do
    context 'proposes discount after order dispatch error' do
      it_behaves_like(
        'saga dispatches command after receiving event',
        Users::Commands::ProposeDiscount,
        Orders::Events::OrderDispatchError
      ) do
        let(:event_params) { { order_id: order.id } }
      end
    end

    context 'reverts payment after discount is rejected' do
      let!(:payment) { Repos::PaymentsRepo.new.create(order: order, finished: true) }

      it_behaves_like(
        'saga dispatches command after receiving event',
        Payments::Commands::RevertPayment,
        Users::Events::DiscountRejected
      ) do
        let(:event_params) { { order_id: order.id } }
      end
    end
  end
end
