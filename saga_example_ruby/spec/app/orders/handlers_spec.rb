require 'spec_helper'

require 'repos'
require 'orders/commands'
require 'orders/events'
require 'orders/exceptions'
require 'orders/handlers'

RSpec.describe 'Orders handlers' do
  let(:user) { Repos::UsersRepo.new.create }
  let(:product) { Repos::ProductsRepo.new.create }
  let(:bus) { Bus.instance }

  after { bus.clean }
  after { Database.instance.clean }

  describe Orders::Handlers::MakeOrderHandler do
    it 'makes order' do
      bus.register(Orders::Commands::MakeOrder, Orders::Handlers::MakeOrderHandler)

      expect { bus.dispatch(Orders::Commands::MakeOrder.new(name: 'foo-bar', user_id: user.id, product_id: product.id)) }.to change { Repos::OrdersRepo.new.all.count }.by(1)
    end

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Orders::Events::OrderMade,
      Orders::Commands::MakeOrder,
      Orders::Handlers::MakeOrderHandler
    ) do
      let(:command_params) { { name: 'foo-bar', user_id: user.id, product_id: product.id } }
    end
  end

  describe Orders::Handlers::ConfirmOrderHandler do
    context 'when product is availible' do
      let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }

      it 'confirms orders' do
        bus.register(Orders::Commands::ConfirmOrder, Orders::Handlers::ConfirmOrderHandler)

        expect { bus.dispatch(Orders::Commands::ConfirmOrder.new(order_id: order.id)) }.to(
          change { Repos::OrdersRepo.new.find(order.id).confirmed }.from(false).to(true)
        )
      end
    end

    context 'when product is not availible' do
      let(:product) { Repos::ProductsRepo.new.create(availible: false) }
      let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }

      it 'confirms orders' do
        bus.register(Orders::Commands::ConfirmOrder, Orders::Handlers::ConfirmOrderHandler)

        expect { bus.dispatch(Orders::Commands::ConfirmOrder.new(order_id: order.id)) }.to(
          raise_error(Orders::Exceptions::ProductNotAvailible)
        )
      end
    end
  end

  describe Orders::Handlers::DispatchOrderHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let!(:payment) { Repos::PaymentsRepo.new.create(order: order, finished: true) }

    context 'when product is availible' do
      it 'dispatches order' do
        bus.register(Orders::Commands::DispatchOrder, Orders::Handlers::DispatchOrderHandler)

        expect { bus.dispatch(Orders::Commands::DispatchOrder.new(order_id: order.id)) }.to(
          change { Repos::OrdersRepo.new.reload(order).dispatched }.from(false).to(true)
        )
      end

      it_behaves_like(
        'dispatches event, after receiving command, though handler',
        Orders::Events::OrderDispatched,
        Orders::Commands::DispatchOrder,
        Orders::Handlers::DispatchOrderHandler
      ) do
        let(:command_params) { { order_id: order.id } }
      end
    end

    context 'when product is not availible' do
      let(:product) { Repos::ProductsRepo.new.create(availible: false) }

      it_behaves_like(
        'dispatches event, after receiving command, though handler',
        Orders::Events::OrderDispatchError,
        Orders::Commands::DispatchOrder,
        Orders::Handlers::DispatchOrderHandler
      ) do
        let(:command_params) { { order_id: order.id } }
      end
    end
  end
end
