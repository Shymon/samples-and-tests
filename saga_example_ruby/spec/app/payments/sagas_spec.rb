require 'spec_helper'

require 'payments/sagas'

RSpec.describe 'Payments sagas' do
  let(:bus) { Bus.instance }
  let(:user) { Repos::UsersRepo.new.create }
  let(:product) { Repos::ProductsRepo.new.create(availible: false) }
  let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
  let!(:payment) { Repos::PaymentsRepo.new.create(order: order) }
  let(:saga) { OrderSaga.new }

  after { bus.clean }
  after { Database.instance.clean }

  describe Payments::Sagas::Charging do
    let(:bus) { Bus.instance }
    let(:user) { Repos::UsersRepo.new.create }
    let(:product) { Repos::ProductsRepo.new.create(availible: false) }
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let!(:payment) { Repos::PaymentsRepo.new.create(order: order) }
    let(:saga) { Payments::Sagas::Charging.new }

    after { bus.clean }
    after { Database.instance.clean }

    context 'after PaymentSucceded, dispatched ChargeFunds' do
      it_behaves_like(
        'saga dispatches command after receiving event',
        Users::Commands::ChargeFunds,
        Payments::Events::PaymentSucceeded
      ) do
        let(:event_params) { { payment_id: payment.id } }
      end
    end

    context 'after FundsCharged, dispatches FinishPayment' do
      it_behaves_like(
        'saga dispatches command after receiving event',
        Payments::Commands::FinishPayment,
        Users::Events::FundsCharged
      ) do
        let(:event_params) { { payment_id: payment.id, current_balance: user.funds } }
      end
    end

    context 'after PaymentReverted, dispatches ReturnFunds' do
      it_behaves_like(
        'saga dispatches command after receiving event',
        Users::Commands::ReturnFunds,
        Payments::Events::PaymentReverted
      ) do
        let(:event_params) { { payment_id: payment.id } }
      end
    end
  end
end
