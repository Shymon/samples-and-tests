require 'spec_helper'

require 'repos'
require 'payments/commands'
require 'payments/events'
require 'payments/exceptions'
require 'payments/handlers'

RSpec.describe 'Payments handlers' do
  let(:user) { Repos::UsersRepo.new.create }
  let(:product) { Repos::ProductsRepo.new.create }
  let(:bus) { Bus.instance }

  after { bus.clean }
  after { Database.instance.clean }

  describe Payments::Handlers::StartPaymentHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }

    it 'creates payment' do
      bus.register(Payments::Commands::StartPayment, Payments::Handlers::StartPaymentHandler)

      expect { bus.dispatch(Payments::Commands::StartPayment.new(order_id: order.id)) }.to(
        change { Repos::PaymentsRepo.new.all.count }.by(1)
      )
    end

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Payments::Events::PaymentStarted,
      Payments::Commands::StartPayment,
      Payments::Handlers::StartPaymentHandler
    ) do
      let(:command_params) { { order_id: order.id } }
    end
  end

  describe Payments::Handlers::SucceedPaymentHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let(:payment) { Repos::PaymentsRepo.new.create(order: order) }

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Payments::Events::PaymentSucceeded,
      Payments::Commands::SucceedPayment,
      Payments::Handlers::SucceedPaymentHandler
    ) do
      let(:command_params) { { payment_id: payment.id } }
    end
  end

  describe Payments::Handlers::FinishPaymentHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let(:payment) { Repos::PaymentsRepo.new.create(order: order) }

    it 'finishes payment' do
      bus.register(Payments::Commands::FinishPayment, Payments::Handlers::FinishPaymentHandler)

      expect { bus.dispatch(Payments::Commands::FinishPayment.new(payment_id: payment.id)) }.to(
        change { Repos::PaymentsRepo.new.reload(payment).finished }.from(false).to(true)
      )
    end

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Payments::Events::PaymentFinished,
      Payments::Commands::FinishPayment,
      Payments::Handlers::FinishPaymentHandler
    ) do
      let(:command_params) { { payment_id: payment.id } }
    end
  end

  describe Payments::Handlers::RevertPaymentHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let(:payment) { Repos::PaymentsRepo.new.create(order: order, finished: true) }

    it 'reverts payment' do
      bus.register(Payments::Commands::RevertPayment, Payments::Handlers::RevertPaymentHandler)

      expect { bus.dispatch(Payments::Commands::RevertPayment.new(payment_id: payment.id)) }.to(
        change { Repos::PaymentsRepo.new.reload(payment).reverted }.from(false).to(true)
      )
    end

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Payments::Events::PaymentReverted,
      Payments::Commands::RevertPayment,
      Payments::Handlers::RevertPaymentHandler
    ) do
      let(:command_params) { { payment_id: payment.id } }
    end
  end

  describe Payments::Handlers::FailPaymentHandler do
    let(:order) { Repos::OrdersRepo.new.create(product: product, user: user) }
    let(:payment) { Repos::PaymentsRepo.new.create(order: order) }

    it 'finishes payment' do
      bus.register(Payments::Commands::FailPayment, Payments::Handlers::FailPaymentHandler)

      expect { bus.dispatch(Payments::Commands::FailPayment.new(payment_id: payment.id)) }.to(
        change { Repos::PaymentsRepo.new.reload(payment).failed }.from(false).to(true)
      )
    end

    it 'fails payment' do
      bus.register(Payments::Commands::FailPayment, Payments::Handlers::FailPaymentHandler)

      expect { bus.dispatch(Payments::Commands::FailPayment.new(payment_id: payment.id)) }.to(
        change { Repos::PaymentsRepo.new.reload(payment).finished }.from(false).to(true)
      )
    end

    it_behaves_like(
      'dispatches event, after receiving command, though handler',
      Payments::Events::PaymentFailed,
      Payments::Commands::FailPayment,
      Payments::Handlers::FailPaymentHandler
    ) do
      let(:command_params) { { payment_id: payment.id } }
    end
  end
end