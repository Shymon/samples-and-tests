$LOAD_PATH << File.join(File.dirname(__dir__), 'app')

require 'bundler'

Bundler.require(:default)
if [nil, '', 'spec', 'spec/'].include?(ARGV[0])
  SimpleCov.start
  SimpleCov.formatter = SimpleCov::Formatter::Console
end

RSpec.shared_examples 'saga dispatches command after receiving event' do |command_class, event_class|
  let(:event_params) { {} } unless method_defined?(:event_params)

  after { bus.clean }

  it "saga dispatches #{command_class} after receiving #{event_class}" do
    command_handler = double("#{command_class}_handler")
    bus.register(command_class, command_handler)
    saga.start

    expect(command_handler).to receive :call
    bus.dispatch(event_class.new(**event_params))
  end
end

RSpec.shared_examples 'dispatches event, after receiving command, though handler' do |event_class, command_class, command_handler_class|
  let(:command_params) { {} } unless method_defined?(:command_params)

  after { bus.clean }

  it "dispatches #{event_class}, after receiving #{command_class} through #{command_handler_class}" do
    bus.register(command_class, command_handler_class)
    event_handler = double("#{event_class}_handler")
    bus.register(event_class, event_handler)

    expect(event_handler).to receive :call
    bus.dispatch(command_class.new(**command_params))
  end
end
