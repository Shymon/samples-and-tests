$LOAD_PATH << File.join(File.dirname(__FILE__), 'app')
require_relative 'terminal_app'

trap 'SIGINT' do
  exit 130
end

if ARGV[0].nil?
  TerminalApp.new.run
  return
end

case ARGV[0].to_s.to_sym
when :terminal
  TerminalApp.new.run
when :'terminal-no-events'
  TerminalApp.new(no_events: true).run
else
  raise NotImplementedError, ARGV[0]
end
