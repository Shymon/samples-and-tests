class String
  # colorization
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def yellow
    colorize(33)
  end

  def blue
    colorize(34)
  end

  def pink
    colorize(35)
  end

  def light_blue
    colorize(36)
  end

  def cycle_color(index)
    colors = %i[red green yellow blue pink light_blue]

    send colors[index % colors.length]
  end

  def colored_length
    length - scan("\e[0m").count * 9
  end

  def cljust(distance, char)
    self + char * [distance - colored_length, 0].max
  end
end
