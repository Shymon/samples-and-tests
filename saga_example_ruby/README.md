# Project to learn Event Driven Development
* Communication via events and commands
* Sagas to manage more complicated, cross context processes
* Frontend in terminal app (check it out!)
* 100% Test coverage - Finished in 0.0338 seconds (files took 0.39107 seconds to load) :)
* Repository pattern
* In-memory database
* No *big* gems used!

By [Szymon Ruta](gitlab.com/shymon)